#include <iostream>
#include <vector>
#include <algorithm>
#include <random>

uint64_t MaxPairwiseProduct(const std::vector<unsigned int>& numbers) {
    unsigned int max1(0), max2(0);

    for (const auto& val : numbers) {
      if (val >= max1) {
        max2 = max1;
        max1 = val;
      } else if (val > max2) {
        max2 = val;
      }
    }

    return static_cast<uint64_t>(max1) *
           static_cast<uint64_t>(max2);
}

uint64_t MaxPairwiseProductNaive(const std::vector<unsigned int>& numbers) {
    uint64_t max_product = 0;
    int n = numbers.size();

    for (int first = 0; first < n; ++first) {
        for (int second = first + 1; second < n; ++second) {
            max_product = std::max(max_product,
                static_cast<uint64_t>(numbers[first]) *
                static_cast<uint64_t>(numbers[second]));
        }
    }

    return max_product;
}

void stressTest(const unsigned int N, const unsigned int M) {
  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<unsigned int> size_gen(2, N);
  std::uniform_int_distribution<unsigned int> num_gen(0, M);
  while(true) {
    // random integer between 2 and N
    unsigned int size(size_gen(gen));
    std::vector<unsigned int> vec(size);
    for (unsigned int idx = 0; idx < size; ++idx) {
      // random integer between 0 and M
      int num(num_gen(gen));
      vec[idx] = num;
      std::cout << num << " ";
    }
    std::cout << std::endl;
    uint64_t res1(MaxPairwiseProduct(vec));
    uint64_t res2(MaxPairwiseProductNaive(vec));
    if (res1 == res2) { std::cout << "OK" << std::endl; }
    else {
      std::cout << "FAILURE: " << res1 << " vs " << res2 << std::endl;
      break;
    }
  }
}

int main() {
    bool test(false);
    if (test) {
        //stressTest(2e5, 2e5);
        stressTest(10, 2e5);
    } else {
        int n;
        std::cin >> n;
        std::vector<unsigned int> numbers(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> numbers[i];
        }

        uint64_t result = MaxPairwiseProduct(numbers);
        std::cout << result << "\n";
    }
    return 0;
}
