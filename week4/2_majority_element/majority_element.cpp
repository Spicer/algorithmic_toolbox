#include <algorithm>
#include <iostream>
#include <vector>
#include <cassert>
#include <random>
#include <benchmark.h>
#include <functional>

using std::vector;

int get_majority_naive(const vector<int>& a) {
  int thresh = a.size() / 2;
  for (size_t ii = 0; ii < a.size(); ++ii) {
    int count(0);
    for (size_t jj = 0; jj < a.size(); ++jj) {
      if (a[ii] == a[jj] && ii != jj) { ++count; }
    }
    if (count > thresh) { return a[ii]; }
  }
  return -1;
}

int get_majority_element_impl(const vector<int>& a, int left, int right) {
  if (left == right) return -1;
  if (left + 1 == right) return a[left];
  //write your code here
  int mid((left + right) / 2);
  int majority1(get_majority_element_impl(a, left, mid));
  int majority2(get_majority_element_impl(a, mid, right));
  int count(0);
  //std::cerr << left << " " << mid << " " << right << std::endl;
  //std::cerr << majority1 << " " << majority2 << std::endl;
  if (majority1 > 0) {
    for (int ii = left; ii < right; ++ii) {
      if (a[ii] == majority1) { ++count; }
    }
    //std::cerr << "count: " << count << std::endl;
    if (count > ((right - left) / 2)) { return majority1; }
    else { count = 0; }
  }
  if (majority2 > 0) {
    for (int ii = left; ii < right; ++ii) {
      if (a[ii] == majority2) { ++count; }
    }
    //std::cerr << "count: " << count << std::endl;
    if (count > ((right - left) / 2)) { return majority2; }
  }
  return -1;
}
int get_majority_element(const vector<int>& a) {
  return get_majority_element_impl(a, 0, a.size());
}

void print_vec(const vector<int>& a) {
  for (const auto& val : a) {
    std::cerr << val << " ";
  }
  std::cerr << std::endl;
}
void test() {
  vector<int> a{2, 3, 9, 2, 2};
  assert(get_majority_element(a) == 2);
  assert(get_majority_element({1, 2, 3, 4}) == -1);
  assert(get_majority_element({1, 2, 3, 1}) == -1);
}
void stress_test(int size) {
  std::mt19937 gen(0); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<int> dis(1, 1e9);
  while(true) {
    vector<int> a(size);
    for (auto& val : a) { val = dis(gen); }
    int key(dis(gen));
    //std::cerr << "key: " << key << std::endl;
    //print_vec(a);
    if (get_majority_element(a) != get_majority_naive(a)) {
      std::cerr << "FAILURE:" << std::endl;
      std::cerr << "key: " << key << std::endl;
      print_vec(a);
    } else { std::cerr << "correct :)" << std::endl; }
  }
}
void print_results(const std::pair<double, double>& time,
                   const std::string& descriptor) {
  std::cout << "Time to find majority element (" << descriptor << ") = "
            << time.first*1e3 << " +- " << time.second*1e3 << " ns"
            << std::endl;
}
void benchmarks() {
  const size_t vec_size(1e5);
  std::vector<int> a(vec_size);
  std::mt19937 gen(0); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<int> dis(1, 1e9);
  for (auto& val : a) { val = dis(gen); }
  std::pair<double, double> time;
  // bench recursive
  time = bench_func<std::function<int()> >(
         std::bind(get_majority_element, a));
  print_results(time, "my impl");

  // bench naive
  //time = bench_func<std::function<int()> >(
  //       std::bind(get_majority_naive, a));
  //print_results(time, "naive impl");
}

int main() {
  //test();
  //stress_test(1e3);
  //benchmarks();

  int n;
  std::cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); ++i) {
    std::cin >> a[i];
  }
  std::cout << (get_majority_element(a) != -1) << '\n';
}
