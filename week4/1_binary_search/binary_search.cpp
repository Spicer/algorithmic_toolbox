#include <iostream>
#include <vector>
#include <cassert>
#include <random>

using std::vector;

int binary_search(const vector<int>& a, int x) {
  int left = 0, right = (int)a.size(), mid(0);
  while(left <= right) {
    mid = left + (right - left) / 2;
    if (x > a[mid]) {
      left = mid + 1;
    } else if (x < a[mid]) {
      right = mid - 1;
    } else {
      return mid;
    }
    //std::cerr << "left=" << left << ", mid=" << mid <<", right=" << right << std::endl;
  }
  return -1;
  //write your code here
}

int linear_search(const vector<int> &a, int x) {
  for (size_t i = 0; i < a.size(); ++i) {
    if (a[i] == x) return i;
  }
  return -1;
}

void print_vec(const vector<int>& a) {
  for (const auto& val : a) {
    std::cerr << val << " ";
  }
  std::cerr << std::endl;
}
void stress_test(int size, int max) {
  std::mt19937 gen(0); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<int> dis(1, max);
  while(true) {
    vector<int> a(size);
    for (auto& val : a) { val = dis(gen); }
    int key(dis(gen));
    //std::cerr << "key: " << key << std::endl;
    //print_vec(a);
    if (binary_search(a, key) != linear_search(a, key)) {
      std::cerr << "FAILURE:" << std::endl;
      std::cerr << "key: " << key << std::endl;
      print_vec(a);
    } else { std::cerr << "correct :)" << std::endl; }
  }
}
void test() {
  vector<int> a{1, 5, 8, 12, 13};
  int key(8);
  assert(binary_search(a, key) == linear_search(a, key));
}

int main() {
  //test();
  //stress_test(1000, 1e9);

  int n;
  std::cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); i++) {
    std::cin >> a[i];
  }
  int m;
  std::cin >> m;
  vector<int> b(m);
  for (int i = 0; i < m; ++i) {
    std::cin >> b[i];
  }
  for (int i = 0; i < m; ++i) {
    //replace with the call to binary_search when implemented
    //std::cerr << linear_search(a, b[i]) << ' ';
    std::cout << binary_search(a, b[i]) << ' ';
  }
}
