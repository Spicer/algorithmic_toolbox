#include <iostream>
#include <vector>
#include <random>
#include <cassert>
#include <benchmark.h>
#include <functional>

using std::vector;

struct Segment {
  int start;
  int end;
};

int get_num_overlap_impl(const vector<Segment>& segments, int point, int left, int right) {
  if ((right - left) == 1) {
    return point >= segments[left].start && point <= segments[left].end;
  } else if ((right - left) < 1) { return 0; }
  int mid = (left + right) / 2;
  //std::cerr << left << ", " << mid << ", " << right << std::endl;
  return get_num_overlap_impl(segments, point, left, mid) + 
         get_num_overlap_impl(segments, point, mid, right);
}
int get_num_overlap(const vector<Segment>& segments, int point) {
  return get_num_overlap_impl(segments, point, 0, segments.size());
}

vector<int> fast_count_segments(const vector<Segment>& segments, const vector<int>& points) {
  //std::cerr << "fast" << std::endl;
  vector<int> cnt(points.size());
  //write your code here
  for (uint32_t ii = 0; ii < points.size(); ++ii) {
    cnt[ii] = get_num_overlap(segments, points[ii]);
  }
  return cnt;
}

vector<int> naive_count_segments(vector<Segment> segments, vector<int> points) {
  //std::cerr << "naive" << std::endl;
  vector<int> cnt(points.size());
  for (size_t i = 0; i < points.size(); i++) {
    for (size_t j = 0; j < segments.size(); j++) {
      cnt[i] += segments[j].start <= points[i] && points[i] <= segments[j].end;
    }
  }
  return cnt;
}

void test() {
  assert(fast_count_segments({{0, 5}, {7, 10}}, {1, 6, 11}) == vector<int>({1, 0, 0}));
  assert(fast_count_segments({{-10, 10}}, {-100, 100, 0}) == vector<int>({0, 0, 1}));
  assert(fast_count_segments({{0, 5}, {-3, 2}, {7, 10}}, {1, 6}) == vector<int>({2, 0}));
}
void stress_test(unsigned int size) {
  std::mt19937 gen(0); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<int> dis(-1e8, 1e8);
  while(true) {
    vector<Segment> segments(size);
    vector<int> points(size);
    for (unsigned int ii = 0; ii < size; ++ii) {
      segments[ii].start = dis(gen);
      segments[ii].end = dis(gen);
      points[ii] = dis(gen);
    }
    //print_vec(a);
    if (naive_count_segments(segments, points) != fast_count_segments(segments, points)) {
      std::cerr << "FAILURE:" << std::endl;
      //print_vec(a);
    } else { std::cerr << "correct :)" << std::endl; }
  }
}

void print_results(const std::pair<double, double>& time,
                   const std::string& descriptor) {
  std::cout << "Time to find number of overlaps (" << descriptor << ") = "
            << time.first*1e3 << " +- " << time.second*1e3 << " ns"
            << std::endl;
}
void benchmarks() {
  const size_t vec_size(1e4);
  std::vector<int> a(vec_size);
  std::mt19937 gen(0); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<int> dis(-1e8, 1e8);
  vector<Segment> segments(vec_size);
  vector<int> points(vec_size);
  for (unsigned int ii = 0; ii < vec_size; ++ii) {
    segments[ii].start = dis(gen);
    segments[ii].end = dis(gen);
    points[ii] = dis(gen);
  }
  std::pair<double, double> time;
  // bench recursive
  time = bench_func<std::function<vector<int>()> >(
         std::bind(fast_count_segments, segments, points), 100);
  print_results(time, "my impl");

  // bench naive
  time = bench_func<std::function<vector<int>()> >(
         std::bind(naive_count_segments, segments, points), 100);
  print_results(time, "naive impl");
}

int main() {
  benchmarks();
  test();
  stress_test(10000);
  int n, m;
  std::cin >> n >> m;
  //vector<int> starts(n), ends(n);
  vector<Segment> segments(n);
  for (size_t i = 0; i < segments.size(); i++) {
    std::cin >> segments[i].start >> segments[i].end;
  }
  vector<int> points(m);
  for (size_t i = 0; i < points.size(); i++) {
    std::cin >> points[i];
  }
  //use fast_count_segments
  vector<int> cnt = fast_count_segments(segments, points);
  for (size_t i = 0; i < cnt.size(); i++) {
    std::cerr << cnt[i] << ' ';
  }
}
