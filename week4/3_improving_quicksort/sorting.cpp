#include <benchmark.h>
#include <functional>
#include <iostream>
#include <vector>
#include <utility>
#include <cassert>
#include <random>
#include <cstdlib>

using std::vector;
using std::swap;

int partition2(vector<int> &a, int l, int r) {
  int x(a[l]);
  int j(l);
  for (int i = l + 1; i <= r; ++i) {
    if (a[i] <= x) { swap(a[i], a[++j]); }
  }
  swap(a[l], a[j]);
  return j;
}
std::pair<int, int> partition3(vector<int> &a, int l, int r) {
  int j(partition2(a, l, r));
  int x(a[l]);
  int k(j);
  for (int i = l + 1; i < j; ++i) {
    if (a[i] == x) { swap(a[i], a[--j]); }
  }
  return std::make_pair(j, k);
}

void randomized_quick_sort(vector<int> &a, int l, int r) {
  while (l < r) {
    int k = l + rand() % (r - l + 1);
    swap(a[l], a[k]);
    std::pair<int, int> res(partition3(a, l, r));

    if ((res.first - l) < (r - res.second)) {
      randomized_quick_sort(a, l, res.first - 1);
      l = res.second + 1;
    } else {
      randomized_quick_sort(a, res.second + 1, r);
      r = res.first - 1;
    }
  }
}

void print_results(const std::pair<double, double>& time,
                   const std::string& descriptor) {
  std::cout << "Time to sort vector (" << descriptor << ") = "
            << time.first*1e3 << " +- " << time.second*1e3 << " ns"
            << std::endl;
}
void bench() {
  const size_t vec_size(1e5);
  std::vector<int> a(vec_size);
  std::mt19937 gen(0); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<int> dis(1, 1e9);
  for (auto& val : a) { val = dis(gen); }
  std::pair<double, double> time;
  // bench normal case
  time = bench_func<std::function<void()> >(
         std::bind(randomized_quick_sort, a, 0, a.size()), 10000);
  print_results(time, "random values");

  // bench all same value case
  int num(dis(gen));
  for (auto& val : a) { val = num; }
  time = bench_func<std::function<void()> >(
         std::bind(randomized_quick_sort, a, 0, a.size()), 10000);
  print_results(time, "all same value");
}

int main() {
  bench();
  int n;
  std::cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); ++i) {
    std::cin >> a[i];
  }
  randomized_quick_sort(a, 0, a.size() - 1);
  for (size_t i = 0; i < a.size(); ++i) {
    std::cout << a[i] << ' ';
  }
}
