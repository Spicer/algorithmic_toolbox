#include <iostream>
#include <vector>

using std::vector;

void print_vec(const vector<int>& vec) {
  for (const auto& val : vec) {
    std::cerr << val << " ";
  }
  std::cerr << std::endl;
}

long long merge(vector<int> &a, vector<int> &b, size_t left, size_t mid, size_t right) {
  //std::cerr << "a" << std::endl;
  //print_vec(a);
  //std::cerr << "b" << std::endl;
  //print_vec(b);
  long long num_inversions(0);
  size_t ii(left), jj(mid);
  while (left < mid && jj < right) {
    if (a[left] <= a[jj]) {
      b[ii++] = a[left++];
    } else {
      b[ii++] = a[jj++];
      num_inversions += mid - left;
    }
  }
  while (left < mid) {
    b[ii++] = a[left++];
    //num_inversions += right - mid;
  }
  while (jj < right) { b[ii++] = a[jj++]; }
  //std::cerr << "post sort" << std::endl;
  //std::cerr << "a" << std::endl;
  //print_vec(a);
  //std::cerr << "b" << std::endl;
  //print_vec(b);
  return num_inversions;
}
long long get_number_of_inversions_impl(vector<int> &a, vector<int> &b, size_t left, size_t right) {
  long long number_of_inversions = 0;
  if (right <= left + 1) return number_of_inversions;
  size_t mid = (right + left) / 2;
  number_of_inversions += get_number_of_inversions_impl(a, b, left, mid);
  number_of_inversions += get_number_of_inversions_impl(b, a, mid, right);
  //write your code here
  //std::cerr << "indices: " << left << " " << right << std::endl;
  number_of_inversions += merge(a, b, left, mid, right);
  //std::cerr << "inversions: " << number_of_inversions << std::endl;
  return number_of_inversions;
}
long long get_number_of_inversions(vector<int> &a) {
  vector<int> b(a);
  return get_number_of_inversions_impl(a, b, 0, a.size());
}

void test() {
  vector<int> a{2, 3, 2, 9, 2};
  std::cerr << get_number_of_inversions(a) << " inversions" << std::endl;

  a = {2, 3, 2, 9, 2, 5};
  std::cerr << get_number_of_inversions(a) << " inversions" << std::endl;
}

int main() {
  //test();
  int n;
  std::cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); i++) {
    std::cin >> a[i];
  }
  std::cout << get_number_of_inversions(a) << '\n';
}
