#include <iostream>
#include <vector>
#include <cassert>
#include <random>
using std::vector;

long long get_fibonacci_partial_sum_naive(long long from, long long to) {
    long long sum = 0;

    long long current = 0;
    long long next  = 1;

    for (long long i = 0; i <= to; ++i) {
        if (i >= from) { sum += current; }
        if (sum >= 10) { sum -= 10; }
        long long new_current = next;
        next = next + current;
        if (next >= 10) { next -= 10; }
        current = new_current;
    }

    return sum % 10;
}

unsigned int fibonacci_sum_last_digit(uint64_t n) {
  if(n <= 1) { return n; }
  unsigned int a    = 0;
  unsigned int b    = 1;
  unsigned int f    = 1;
  unsigned int sum  = 1;
  unsigned int ii(1);
  for( ; ii < n; ++ii) {
    f += a;
    if (f >= 10) { f -= 10; }
    a = b;
    b = f;
    sum += f;
    if (sum >= 10) { sum -= 10; }
    //std::cerr << sum << " ";
    if (a == 0 && b == 1) { break; }
  }
  //std::cerr << std::endl;
  //std::cerr << "ii=" << ii << std::endl;
  //std::cerr << "sum=" << sum << std::endl;
  if (ii == n) { return sum % 10; }
  else { return fibonacci_sum_last_digit(n % ii); }
}
unsigned int partial_fibonacci_sum_last_digit(uint64_t m, uint64_t n) {
  if (m == 0) { return fibonacci_sum_last_digit(n); }
  int res(fibonacci_sum_last_digit(n) - fibonacci_sum_last_digit(m-1));
  if (res < 0) { return static_cast<unsigned int>(res + 10); }
  else { return static_cast<unsigned int>(res); }
}

void stress_test() {
  std::mt19937 gen(0); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<int> dis(1, 1e3);
  while(true) {
    int from(dis(gen));
    int to(dis(gen));
    if (from > to) { std::swap(from, to); }
    std::cerr << "input=" << from << ", " << to << std::endl;
    uint64_t res(partial_fibonacci_sum_last_digit(from, to));
    uint64_t res_naive(get_fibonacci_partial_sum_naive(from, to));
    if (res != res_naive) {
      std::cerr << "Failure: " << res << " vs " <<
                   res_naive << std::endl;
      break;
    } else { std::cerr << "OK" << std::endl; }
  }
}
void test() {
  assert(partial_fibonacci_sum_last_digit(3, 7) == 1);
  assert(partial_fibonacci_sum_last_digit(10, 10) == 5);
  assert(partial_fibonacci_sum_last_digit(10, 200) == 2);
}

int main() {
    long long from, to;
    std::cin >> from >> to;
    //std::cerr << "naive:" << get_fibonacci_partial_sum_naive(from, to) << '\n';
    std::cout << partial_fibonacci_sum_last_digit(from, to) << '\n';
    //test();
    //stress_test();
}
