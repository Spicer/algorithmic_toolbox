#include <iostream>
#include <cassert>

int get_fibonacci_last_digit_naive(int n) {
  if (n <= 1)
    return n;

  int previous = 0;
  int current  = 1;

  for (int i = 0; i < n - 1; ++i) {
    int tmp_previous = previous;
    previous = current;
    current = tmp_previous + current;
  }

  return current % 10;
}

unsigned int get_fibonacci_last_digit(unsigned int n) {
  if (n <= 1) {
    return n;
  }

  uint8_t previous(0);
  uint8_t current(1);

  for (unsigned int ii = 0; ii < n - 1; ++ii) {
    uint8_t tmp_previous = previous;
    previous = current;
    current = (tmp_previous + current) % 10;
  }

  return current % 10;
}

void test() {
  assert(get_fibonacci_last_digit(50) == 5);
  assert(get_fibonacci_last_digit(200) == 5);
  assert(get_fibonacci_last_digit(331) == 9);
  assert(get_fibonacci_last_digit(327305) == 5);
  assert(get_fibonacci_last_digit(1e7) == 5);
}

int main() {
  unsigned int n;
  std::cin >> n;
  unsigned int c = get_fibonacci_last_digit(n);
  std::cout << c << '\n';
  //test();
}
