#include <iostream>
#include <cassert>
#include <random>

/*
 * @author Matt Spicer
 * @date 10-14-2018
 *
 * @brief Compute the least common multiple of two numbers
 */

unsigned long long lcm_naive(int a, int b) {
  for (long l = 1; l <= (long long) a * b; ++l)
    if (l % a == 0 && l % b == 0)
      return l;

  return (unsigned long long) a * b;
}

uint64_t lcm_faster(unsigned int a, unsigned int b) {
  for (unsigned int l = std::max(1u, b / a); l <= b;) {
    uint64_t num(static_cast<uint64_t>(l) * static_cast<uint64_t>(a));
    if (num % b == 0) { return num; }
    else { l += a / (b % a); }
  }

  return static_cast<uint64_t>(a) *
         static_cast<uint64_t>(b);
}

int gcd(int a, int b) {
  if (b == 0) { return a; }
  return gcd(b, a % b);
}
uint64_t lcm(unsigned int a, unsigned int b) {
  return static_cast<uint64_t>(b) *
         static_cast<uint64_t>(a) /
         gcd(a, b);
}

void test() {
  assert(lcm_naive(750, 1000) == lcm(750, 1000));
  assert(lcm_naive(6, 8) == lcm(6, 8));
  assert(lcm_naive(28851538, 1183019) == lcm(28851538, 1183019));
}
void stress_test(unsigned int max) {
  std::mt19937 gen(0); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<int> dis(1, max);
  while(true) {
    int num1(dis(gen));
    int num2(dis(gen));
    std::cout << "input=" << num1 << ", " << num2 << std::endl;
    uint64_t res(lcm(num1, num2));
    uint64_t res_naive(lcm_naive(num1, num2));
    if (res != res_naive) {
      std::cout << "Failure: " << res << " vs " <<
                   res_naive << std::endl;
      break;
    } else { std::cout << "OK" << std::endl; }
  }
}

int main() {
  int a, b;
  std::cin >> a >> b;
  std::cout << lcm(a, b) << std::endl;
  //std::cout << lcm_naive(a, b) << std::endl;
  //test();
  //stress_test(1e3);
  return 0;
}
