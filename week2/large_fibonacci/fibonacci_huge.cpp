#include <iostream>

long long get_fibonacci_huge_naive(long long n, long long m) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;

    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
    }

    return current % m;
}

uint64_t get_fibonacci_huge(uint64_t n, uint64_t m) {
  if(n <= 1) { return n; }
  uint64_t a = 0;
  uint64_t b = 1;
  uint64_t f = 1;
  unsigned int ii(1);
  for( ; ii < n; ++ii) {
    f += a;
    if (f >= m) { f -= m; }
    a = b;
    b = f;
    if (a == 0 && b == 1) { break; }
    //std::cerr << f << " ";
  }
  //std::cerr << std::endl;
  //std::cerr << ii << std::endl;
  if (ii == n) { return f % m; }
  else { return get_fibonacci_huge(n % ii, m); }
}

int main() {
    long long n, m;
    std::cin >> n >> m;
    std::cout << get_fibonacci_huge(n, m) << '\n';
}
