#include <iostream>
#include <random>
#include <cassert>

/*
 * @brief Calculate the last digit of the sum of fibonacci numbers
 */
unsigned int fibonacci(unsigned int n) {
  if (n <= 1) { return n; }

  long long previous = 0;
  long long current  = 1;

  for (long long i = 0; i < n - 1; ++i) {
    long long tmp_previous = previous;
    previous = current;
    current = tmp_previous + current;
  }

  return current % 10;
}

int fibonacci_sum_naive(long long n) {
  if (n <= 1)
    return n;

  long long previous = 0;
  long long current  = 1;
  long long sum      = 1;

  for (long long i = 0; i < n - 1; ++i) {
    long long tmp_previous = previous;
    previous = current;
    current = tmp_previous + current;
    if (current >= 10) { current -= 10; }
    sum += current;
    if (sum >= 10) { sum -= 10; }
  }

  return sum % 10;
}

unsigned int fibonacci_sum_last_digit(uint64_t n) {
  if(n <= 1) { return n; }
  uint64_t a    = 0;
  uint64_t b    = 1;
  uint64_t f    = 1;
  uint64_t sum  = 1;
  unsigned int ii(1);
  for( ; ii < n; ++ii) {
    f += a;
    if (f >= 10) { f -= 10; }
    a = b;
    b = f;
    sum += f;
    if (sum >= 10) { sum -= 10; }
    //std::cerr << sum << " ";
    if (a == 0 && b == 1) { break; }
  }
  //std::cerr << std::endl;
  //std::cerr << "ii=" << ii << std::endl;
  if (ii == n) { return sum % 10; }
  else { return fibonacci_sum_last_digit(n % ii); }
}

void stress_test() {
  std::mt19937 gen(0); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<int> dis(1, 1e3);
  while(true) {
    int num(dis(gen));
    std::cerr << "input=" << num << std::endl;
    uint64_t res(fibonacci_sum_last_digit(num));
    uint64_t res_naive(fibonacci_sum_naive(num));
    if (res != res_naive) {
      std::cerr << "Failure: " << res << " vs " <<
                   res_naive << std::endl;
      break;
    } else { std::cerr << "OK" << std::endl; }
  }
}

void test() {
  assert(fibonacci_sum_last_digit(3) == 4);
  assert(fibonacci_sum_last_digit(100) == 5);
  assert(fibonacci_sum_last_digit(549) == 4);
}

int main() {
  long long n = 0;
  std::cin >> n;
  //std::cerr << "naive: " << fibonacci_sum_naive(n) << std::endl;
  std::cout << fibonacci_sum_last_digit(n) << std::endl;
  //test();
  //stress_test();
}
