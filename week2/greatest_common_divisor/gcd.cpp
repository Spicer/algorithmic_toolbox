#include <iostream>
#include <cassert>

/*
 * @author Matt Spicer
 * @date 10-14-2018
 *
 * @brief Compute the greatest common divisor of two numbers
 */

int gcd_naive(int a, int b) {
  int current_gcd = 1;
  for (int d = 2; d <= a && d <= b; d++) {
    if (a % d == 0 && b % d == 0) {
      if (d > current_gcd) {
        current_gcd = d;
      }
    }
  }
  return current_gcd;
}

int gcd(int a, int b) {
  if (b == 0) { return a; }
  return gcd(b, a % b);
}

void test() {
  assert(gcd_naive(10, 15) == gcd(10, 15));
  assert(gcd_naive(1234, 5678) == gcd(1234, 5678));
  assert(gcd_naive(123456789, 567891234) == gcd(123456789, 567891234));
}

int main() {
  int a, b;
  std::cin >> a >> b;
  std::cout << gcd(a, b) << std::endl;
  //test();
  return 0;
}
