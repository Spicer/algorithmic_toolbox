#include <iostream>

/*
 * @brief Find the minimum number of coins to produce the given change
 * Use only coins worth 10, 5, or 1
 */
int get_change(int m) {
  int coins(0);
  while (m > 0) {
    if (m >= 10) {
      ++coins;
      m -= 10;
    } else if (m >= 5) {
      ++coins;
      m -= 5;
    } else if (m >= 1) {
      ++coins;
      m -= 1;
    }
  }
  return coins;
}

int main() {
  int m;
  std::cin >> m;
  std::cout << get_change(m) << '\n';
}
