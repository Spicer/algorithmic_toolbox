#include <algorithm>
#include <iostream>
#include <climits>
#include <vector>

using std::vector;

struct Segment {
  int start, end;
};

void print_segments(const std::vector<Segment>& segments) {
  for (const auto& seg : segments) {
    std::cerr << seg.start << ", " << seg.end << std::endl;
  }
}

vector<int> optimal_points(vector<Segment> &segments) {
  //print_segments(segments);
  std::sort(segments.begin(), segments.end(),
            [](const Segment& s1, const Segment& s2) {
              return s1.end < s2.end;
            });
  //print_segments(segments);
  vector<int> points;
  //write your code here
  size_t idx(0);
  while (idx < segments.size()) {
    int point(segments[idx].end);
    points.push_back(point);
    do { ++idx; } while (idx < segments.size() && segments[idx].start <= point);
  }
  return points;
}

int main() {
  int n;
  std::cin >> n;
  vector<Segment> segments(n);
  for (size_t i = 0; i < segments.size(); ++i) {
    std::cin >> segments[i].start >> segments[i].end;
  }
  vector<int> points = optimal_points(segments);
  std::cout << points.size() << "\n";
  for (size_t i = 0; i < points.size(); ++i) {
    std::cout << points[i] << " ";
  }
}
