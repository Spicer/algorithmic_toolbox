#include <algorithm>
#include <sstream>
#include <iostream>
#include <vector>
#include <string>
#include <cassert>

using std::vector;
using std::string;

/*
 * @brief Take a vector of numbers (in string form), and concatenate them
 * in order to make the largest possible number
 *
 * @author Matt Spicer
 * @date 10-18-2018
 */

bool is_greater(const std::string& digit, const std::string& max) {
  if (max.size() > digit.size()) { return !is_greater(max, digit); }
  uint16_t idx(0);
  for ( ; idx < max.size(); ++idx) {
    if (digit[idx] < max[idx]) { return false; }
    else if (digit[idx] > max[idx]) { return true; }
  }
  for ( ; idx < digit.size(); ++idx) {
    if (digit[idx] < max[0]) { return false; }
    else if (digit[idx] > max[0]) { return true; }
  }
  return digit.back() > max.back();
}

void print_number(const vector<string>& str) {
  for (const auto& val : str) {
    std::cerr << val;
  }
  std::cerr << std::endl;
}

string largest_number(vector<string> a) {
  //print_number(a);
  std::sort(a.begin(), a.end(), &is_greater);
  //print_number(a);
  std::stringstream ret;
  for (const auto& num : a) { ret << num; }
  return ret.str();
}

void test() {
  for (uint16_t a = 0; a < 1001; ++a) {
    for (uint16_t b = 0; b < 1001; ++b) {
      std::cerr << a << " vs " << b << std::endl;
      if (is_greater(std::to_string(a), std::to_string(b))) {
        assert(std::stoi(std::to_string(a) + std::to_string(b)) >=
               std::stoi(std::to_string(b) + std::to_string(a)));
      } else {
        assert(std::stoi(std::to_string(b) + std::to_string(a)) >=
               std::stoi(std::to_string(a) + std::to_string(b)));
      }
    }
  }
}

int main() {
  //test();
  int n;
  std::cin >> n;
  vector<string> a(n);
  for (size_t i = 0; i < a.size(); i++) {
    std::cin >> a[i];
  }
  std::cout << largest_number(a);
}
