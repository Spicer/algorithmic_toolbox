#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <cassert>

using std::vector;

template<typename T>
void print_vec(const std::vector<T>& vec) {
  for (const auto& val : vec) {
    std::cerr << val << " ";
  }
  std::cerr << std::endl;
}

double get_optimal_value(int capacity, vector<int> weights, vector<int> values) {
  double value(0.0);
  std::vector<double> ratios(weights.size());
  for (size_t ii = 0; ii < ratios.size(); ++ii) {
    ratios[ii] = static_cast<double>(values[ii]) /
                 static_cast<double>(weights[ii]);
  }
  // initialize original index locations
  std::vector<size_t> idx(weights.size());
  std::iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  std::sort(idx.begin(), idx.end(),
       [&ratios](size_t idx1, size_t idx2) {
         return ratios[idx1] > ratios[idx2];
       });
  //print_vec(idx);

  for (size_t ii = 0; ii < weights.size(); ++ii) {
    if (capacity > weights[idx[ii]]) {
      value += static_cast<double>(values[idx[ii]]);
      capacity -= weights[idx[ii]];
    } else {
      value += static_cast<double>(values[idx[ii]]) * 
               static_cast<double>(capacity) /
               static_cast<double>(weights[idx[ii]]);
      break;
    }
    //std::cerr << value << " ";
  }
  //std::cerr << std::endl;

  return value;
}

void test() {
  int capacity(50);
  std::vector<int> weights{20, 50, 30};
  std::vector<int> values{60, 100, 120};
  assert(fabs(get_optimal_value(capacity, weights, values) - 180.0) < 1e-4);
}

int main() {
  //test();
  int n;
  int capacity;
  std::cin >> n >> capacity;
  vector<int> values(n);
  vector<int> weights(n);
  for (int i = 0; i < n; i++) {
    std::cin >> values[i] >> weights[i];
  }

  double optimal_value = get_optimal_value(capacity, weights, values);

  std::cout.precision(10);
  std::cout << optimal_value << std::endl;
  return 0;
}
